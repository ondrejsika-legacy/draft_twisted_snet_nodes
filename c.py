from twisted.internet.protocol import Protocol, ClientFactory
from twisted.internet import reactor
import json

import a

n = a.Node('c', 7003)
n.connect('a', 7001)

reactor.callLater(2, lambda: n.send_message('a', 'b', 'Hello'))

reactor.run()
