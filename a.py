from twisted.internet.protocol import Protocol, Factory, ClientFactory
from twisted.internet import reactor
import json


class P(Protocol):
    def __init__(self, conn_req=None):
        self._conn_req = conn_req

    def connectionMade(self):
        if self._conn_req:
            target, name = self._conn_req
            self.transport.write(json.dumps(['CONNECT', [name, ]]))
            self.node._connections[target] = self

    def dataReceived(self, data):
        type, payload = json.loads(data)
        if type == 'CONNECT':
            name, = payload
            self.node._connections[name] = self
            print self.node._connections
        elif type == 'MSG':
            name, _ = payload
            if self.node._name == name:
                self.node.recieve_msg(payload)
            else:
                self.node._connections[name].transport.write(data)

class Node(Factory, ClientFactory):
    def __init__(self, name, port):
        self._name = name
        self._connections = {}
        self._conn_req = None
        reactor.listenTCP(port, self)

    def buildProtocol(self, addr):
        p = P(self._conn_req)
        p.node = self
        return p

    def recieve_msg(self, msg):
        print '>>', msg

    def connect(self, name, port):
        self._conn_req = name, self._name
        reactor.connectTCP('localhost', port, self)

    def send_message(self, endpoint, name, message):
        self._connections[endpoint].transport.write(json.dumps(['MSG', [name, message]]))

if __name__ == '__main__':

    n = Node('r', 7001)

    reactor.run()
